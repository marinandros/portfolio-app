import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { useEffect } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Route, Switch } from 'react-router-dom'
import { fetchUserAction } from './store/fetchUser'
import Navigation from './containers/Navigation/Navigation'
import {
  getUser, getUserError,
  getUserPending
} from './reducers/userReducer'
import {
  Profile, PageNotFound,
  Home, Contact, User
} from './components'

type AppProps = {
  error: string | null
  user: User | null
  pending: boolean
  fetchUser: () => {}
}

function App(props: AppProps) {
  const { user, fetchUser } = props
  useEffect(() => {
    console.log('loaded here')
    fetchUser()
  }, [fetchUser])

  return (
    <div className="App">
      <Navigation />
      <Switch>
        <Route exact path='/' render={(props) => <Home {...props} isActive={ true } />} />
        <Route exact path='/profile' render={(props) => <Profile user={user} userId={user?.id ?? ''} />} />
        <Route exact path='/contact' component={ Contact } />
        <Route path='/' component={ PageNotFound } />
      </Switch>
    </div>
  );
}

const mapStateToProps = (state: any) => {
  return { error: getUserError(state.user), // App component has access to this through props.error
    user: getUser(state.user),
    pending: getUserPending(state.user)
  }
}

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  fetchUser: fetchUserAction // App component has access to this through props.fetchUser
}, dispatch)

export default connect(
  mapStateToProps, // give Redux state access to App component through App component props
  mapDispatchToProps // give App component ability to dispatch actions
)(App)
