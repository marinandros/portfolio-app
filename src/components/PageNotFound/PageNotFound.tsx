export const PageNotFound = () => {
  return (
    <div>Error 404: Page that you have requested has not been found</div>
  )
}