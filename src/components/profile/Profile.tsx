import React from 'react'
import { ProfileProps } from './interfaces'
import profileStyle from './Profile.module.css'
import UserSkills  from './UserSkills/UserSkills'

export function Profile(props: ProfileProps) {
  const { user } = props
  const generateProfileBlock = () => {
    if (!user) return <p>'Loading user'</p>
    const imageUrl = `${user.username}.png`
    return <div>
      <h1 className={profileStyle.userName}>{user.firstName} {user.lastName}</h1>
      <img src={imageUrl} alt='Developer profileImage'/>
      <p>{user.dob}</p>
      <p>{user.email}</p>
      <p>{user.city}</p>
      <p>{user.state}</p>
      <p>{user.postalCode}</p>
    </div>
  }

  return <React.Fragment>
    {generateProfileBlock()}
    <UserSkills  />
  </React.Fragment>
}
