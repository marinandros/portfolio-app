export type User = {
  id: string
  firstName: string
  lastName: string
  dob: string
  postalCode: number
  city: string
  state: string
  email: string
  username: string
}

export type ProfileProps = {
  user: User | null
  userId: string
}