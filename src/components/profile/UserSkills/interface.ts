type UserSkill = {
  id: string
  displayName: string
  name: string
  isPrimary: boolean
  isSecondary: boolean
  rate: number
  scale: number
  skillId: string
  userId: string
}

export type UserSkills = UserSkill[]