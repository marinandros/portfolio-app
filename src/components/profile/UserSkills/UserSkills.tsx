import { useEffect } from 'react'
import { connect } from 'react-redux'
import { UserSkills } from './interface'
import { bindActionCreators } from 'redux'
import userSkillsStyle from './UserSkills.module.css'
import { fetchUserSkillsAction } from '../../../store/fetchUserSkills'
import {
  getUserSkills,
  getUserSkillsError,
  getUserSkillsPending
} from '../../../reducers/userSkillsReducer'

export type UserSkillsProps = {
  userSkills: UserSkills
  error: string | null
  pending: boolean
  fetchUserSkills: () => {}
}

const UserSkillList = (props: UserSkillsProps) => {
  const { userSkills, fetchUserSkills } = props
  useEffect(() => {
    if (userSkills.length === 0) {
      fetchUserSkills()
    } 
  }, [fetchUserSkills, userSkills])

  const renderUserSkills = () => {
    if (userSkills.length === 0) {
      return <p>No skills to show</p>
    }
    const skillList = userSkills.map((skill) => {
      const { displayName, isPrimary, isSecondary, rate, scale, id } = skill
      const h1ClassName = isPrimary 
      ? userSkillsStyle.primary 
      : (isSecondary ? userSkillsStyle.secondary : userSkillsStyle.default)
      return (
        <div key={id}>
          <h1 className={h1ClassName}>{displayName}</h1>
          <p>Slider numbers {rate}/{scale}</p>
        </div>
        
      )
    })
    return <div>{skillList}</div>
  }
  return renderUserSkills()
}

const mapStateToProps = (state: any) => {
  return {
    error: getUserSkillsError(state.userSkills),
    userSkills: getUserSkills(state.userSkills),
    pending: getUserSkillsPending(state.userSkills)
  }
}

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  fetchUserSkills: fetchUserSkillsAction
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserSkillList)