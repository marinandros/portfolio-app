export type NavigationProps = {
  isTop: boolean;
  isActive: boolean;
  onSidebarActivation: () => void;
  onScrollActivation: () => void;
}