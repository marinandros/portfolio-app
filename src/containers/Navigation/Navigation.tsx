import style from './Navigation.module.css'
import { connect } from 'react-redux'
import { User } from '../../components'
import { Link } from 'react-router-dom'
import { Navbar, Container, Image } from 'react-bootstrap'
import { getUser, getUserError, getUserPending } from '../../reducers'

type NavigationProps = {
  error: string | null
  user: User | null
  pending: boolean
}

const Navigation = (props: NavigationProps) => {
  const navClasses = [style.navItem, 'nav-link']
  const { user } = props

  return (
    <Navbar collapseOnSelect sticky='top' expand='sm' bg='dark' variant='dark'>
      <Container>
      <Navbar.Brand style={{width: '50px'}}>    
        <Image src={user?.username
          ? `${user.username}.svg`
          : 'defaultLogo.png'} fluid/>
      </Navbar.Brand>
        <Navbar.Toggle aria-controls='responsive-navbar-nav'/>
        <Navbar.Collapse className='responsive-navbar-nav'>
          <Link to='/' className={navClasses.join(' ')}> Home</Link>
          <Link to='/profile' className={navClasses.join(' ')}>Profile</Link>
          <Link to='/contact' className={navClasses.join(' ')}>Contact</Link>
        </Navbar.Collapse>
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>
            {user?.firstName} {user?.lastName} Portfolio
          </Navbar.Text>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

const mapStateToProps = (state: any) => {
  return { error: getUserError(state.user), // App component has access to this through props.error
    user: getUser(state.user),
    pending: getUserPending(state.user)
  }
}

export default connect(
  mapStateToProps // give Redux state access to App component through App component props
)(Navigation)