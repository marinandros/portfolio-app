import { AxiosResponse } from 'axios'
import { axiosClient } from '../client'
import { User } from '../components'
import { fetchUserFailure, fetchUserPending, fetchUserSuccess } from './actions'

const userId = process.env.REACT_APP_USER_ID || ''

export const fetchUserAction = () => {
  return (dispatch: any) => {
    dispatch(fetchUserPending())
    axiosClient.get<User>(`/user/${userId}`)
    .then((response: AxiosResponse<User>) => {
      dispatch(fetchUserSuccess(response.data))
    })
    .catch((err: any) => fetchUserFailure(err.message))
  }
}