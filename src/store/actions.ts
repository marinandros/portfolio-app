import * as TYPES from './../types'
import { User, UserSkills } from '../components'
import { UserAction, UserSkillsAction } from '../reducers/interfaces'

export const fetchUserPending = () => {
  return {
    type: TYPES.FETCH_USER_PENDING
  }
}

export const fetchUserSuccess = (user: User): UserAction => {
  return {
    type: TYPES.FETCH_USER_SUCCESS,
    payload: user
  }
}

export const fetchUserFailure = (error: Error): UserAction => {
  return {
    type: TYPES.FETCH_USER_FAILURE,
    error: error.message
  }
}

export const fetchUserSkillsPending = () => {
  return {
    type: TYPES.FETCH_USER_SKILLS_PENDING
  }
}

export const fetchUserSkillsSuccess = (userSkills: UserSkills): UserSkillsAction => {
  return {
    type: TYPES.FETCH_USER_SKILLS_SUCCESS,
    payload: userSkills
  }
}

export const fetchUserSkillsFailure = (error: Error): UserSkillsAction => {
  return {
    type: TYPES.FETCH_USER_SKILLS_FAILURE,
    error: error.message
  }
}