import { AxiosResponse } from 'axios'
import { axiosClient } from '../client'
import { User, UserSkills } from '../components'
import {
  fetchUserSkillsFailure,
  fetchUserSkillsPending,
  fetchUserSkillsSuccess
} from './actions'

const userId = process.env.REACT_APP_USER_ID || ''

type UserSkillsData = User & {
  userSkills: UserSkills
}

export const fetchUserSkillsAction = () => {
  return (dispatch: any) => {
    dispatch(fetchUserSkillsPending())
    axiosClient.get<UserSkillsData>(`/userSkills/${userId}`)
    .then((response: AxiosResponse<UserSkillsData>) => {
      dispatch(fetchUserSkillsSuccess(response.data.userSkills))
    })
    .catch((err: any) => {
      fetchUserSkillsFailure(err.message)
    })
  }
}