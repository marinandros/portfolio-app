import { User, UserSkills } from '../components'

export type UserState = {
  pending: boolean
  user: User | null
  error: string | null
}

export interface UserAction {
  type: Symbol
  payload?: User
  error?: string
}

export type UserSkillsState = {
  pending: boolean
  userSkills: UserSkills
  error: string | null
}

export interface UserSkillsAction {
  type: Symbol
  payload?: UserSkills
  error?: string
}