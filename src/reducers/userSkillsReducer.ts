import * as TYPES from '../types'
import { UserSkillsAction, UserSkillsState } from './interfaces'

const initialState: UserSkillsState = {
  pending: false,
  userSkills: [],
  error: null
}

export const UserSkillsReducer = (state = initialState, action: UserSkillsAction): UserSkillsState => {
  switch (action.type) {
    case TYPES.FETCH_USER_SKILLS_PENDING: 
      return {
          ...state,
          pending: true
      }
    case TYPES.FETCH_USER_SKILLS_SUCCESS:
      return {
          ...state,
          pending: false,
          userSkills: action.payload ?? []
      }
    case TYPES.FETCH_USER_SKILLS_FAILURE:
      return {
          ...state,
          pending: false,
          error: action.error ?? null
      }
    default: 
        return state
  }
}

// Selectors -> functions that allow fetching parts of the defined state
// allow for better scalability of react/redux apps since they become a single
// point of required change on state change
export const getUserSkills = (state: UserSkillsState) => state.userSkills
export const getUserSkillsError = (state: UserSkillsState) => state.error
export const getUserSkillsPending = (state: UserSkillsState) => state.pending