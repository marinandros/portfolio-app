import * as TYPES from '../types'
import { UserAction, UserState } from './interfaces'

const initialState: UserState = {
  pending: false,
  user: null,
  error: null
}

export const UserReducer = (state = initialState, action: UserAction): UserState => {
  switch (action.type) {
    case TYPES.FETCH_USER_PENDING: 
      return {
          ...state,
          pending: true
      }
    case TYPES.FETCH_USER_SUCCESS:
      return {
          ...state,
          pending: false,
          user: action.payload ?? null
      }
    case TYPES.FETCH_USER_FAILURE:
      return {
          ...state,
          pending: false,
          error: action.error ?? null
      }
    default: 
        return state
  }
}

// Selectors -> functions that allow fetching parts of the defined state
// allow for better scalability of react/redux apps since they become a single
// point of required change on state change
export const getUser = (state: UserState) => state.user
export const getUserError = (state: UserState) => state.error
export const getUserPending = (state: UserState) => state.pending